"""
Student Grades
Source: 
"""
import argparse
import time

import operations as ops


# Parse args
parser = argparse.ArgumentParser()
parser.add_argument('--clean', '-c', help='use non-mutated dataset', dest='clean', action='store_true')
parser.add_argument('--unwrapped', '-u', help='do not use r6p; must be used with --clean to work', dest='unwrapped', action='store_true')
args = parser.parse_args()

# Read non-mutated or mutated
filename = 'clean_input.csv' if args.clean else 'mutated_input.csv'

# Set up timing
start_time = time.time()
times = []

# Check if r6p is to be used
if args.unwrapped:
    ops.ops_list = [op.unwrapped if hasattr(op, 'unwrapped') else op for op in ops.ops_list]

# Run through operations
prev_result = filename
for op in ops.ops_list:
    # Execute operation
    result = op(prev_result)
    
    # Time
    times.append(time.time() - start_time)
    
    # Save result for next op
    prev_result = result

# Output result
print(prev_result)


# Output time results
for time in times:
    print(time)
print(f'Total time: {sum(times)}')