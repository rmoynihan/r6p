# Add r6p to PATH
import sys
sys.path.insert(1, '../../')

from itertools import groupby
from operator import itemgetter
from functools import reduce

from r6p.decorator import r6p


def read(filename, delimiter=','):
    with open(filename) as file:
        lines = []
        for line in file.readlines():
            line = line.strip()
            line = line.split(delimiter)
            lines.append(tuple(line))
            
        return lines
    

@r6p
def map1(data):
    return [(elem[0].split(':')[0], int(elem[0].split(':')[1])) for elem in data]


@r6p
def map2(data):
    return [(elem[0] + ' Pass', 1) if elem[1] > 40 else (elem[0] + ' Fail', 1) for elem in data]


def reduce1(data):
    return [reduce(lambda obj1, obj2: (key, obj1[1] + obj2[1]), group) for key, group in groupby(sorted(data), key=lambda x: x[0])]


@r6p
def filter1(data):
    return [elem for elem in data if elem[1] > 5]
    

ops_list = [
    read,
    map1,
    map2,
    reduce1,
    filter1,
]