# Add r6p to PATH
import sys
sys.path.insert(1, '../../')

from r6p.data_generator import DataGenerator
from r6p.data_types import Integer, Float, String, Date, Time, Concat

# Make a clean and mutated version for each project
for i, fname in enumerate(['clean_input.csv', 'mutated_input.csv']):
    # project1
    dg = DataGenerator([
        String(prefix="123", string_len=7),
        Integer(0, 1000),
        String(string_type="sentence"),
        Float(0.0, 1.0),
    ])
    
    dg.generate_data(n_records=10000, filename=f'../p1/{fname}', m_factor=0.2 * i, create_metadata=bool(i))


    # find_salary
    dg = DataGenerator([
        String(prefix="$", char_type="numeric_only", string_len=3),
    ])
    
    dg.generate_data(n_records=10000, filename=f'../find_salary/{fname}', m_factor=0.2 * i, create_metadata=bool(i))


    # airport_transit
    dg = DataGenerator([
        Date(date_format="%m/%d/%Y"),
        String(char_type='numeric_only', string_len=6),
        Time(),
        Time(),
        String(char_type="alpha_only", string_len=3, prefix='M')
    ])
    
    dg.generate_data(n_records=10000, filename=f'../airport_transit/{fname}', m_factor=0.2 * i, create_metadata=bool(i))


    # student_grades
    dg = DataGenerator([
        Concat([
            String(prefix="CS", string_len=2),
            Integer(min_val=900, max_val=999),
            ':',
            Integer(min_val=0, max_val=100)
        ]),
        Integer(min_val=0, max_val=100),
        Integer(min_val=0, max_val=100),
        Integer(min_val=0, max_val=100),
    ])
    
    dg.generate_data(n_records=10000, filename=f'../student_grades/{fname}', m_factor=0.2 * i, create_metadata=bool(i))