# Add r6p to PATH
import sys
sys.path.insert(1, '../../')

from r6p.decorator import r6p


def read(filename, delimiter=','):
    with open(filename) as file:
        lines = []
        for line in file.readlines():
            line = line.strip()
            line = line.split(delimiter)
            lines.append(tuple(line))
            
        return lines
    
@r6p
def map1(data):
    return [(elem[0][1:],) if '$' in elem[0][0] else (elem[0],) for elem in data]

@r6p
def map2(data):
    return [(int(elem[0]),) for elem in data]

@r6p
def filter1(data):
    return [(elem[0],) for elem in data if elem[0] < 300]

@r6p
def reduce_transformer1(data):
    return [elem[0] for elem in data]

def reduce1(data):
    return sum(reduce_transformer1(data))

ops_list = [
    read,
    map1,
    map2,
    filter1,
    reduce1,
]