# Add r6p to PATH
import sys
sys.path.insert(1, '../../')

from itertools import groupby
from operator import itemgetter
from functools import reduce

from r6p.decorator import r6p


def read(filename, delimiter=','):
    with open(filename) as file:
        lines = []
        for line in file.readlines():
            line = line.strip()
            line = line.split(delimiter)
            lines.append(tuple(line))
            
        return lines
    

@r6p
def map1(data):
    return [(elem[0], elem[1], elem[2], elem[3], elem[4], elem[2].split(':')[0]) for elem in data]


@r6p
def map2(data):
    return [(
        elem[0],
        elem[1],
        elem[2],
        elem[3],
        elem[4],
        elem[5],
        (abs(60*(int(elem[3].split(':')[0]) - int(elem[2].split(':')[0])))) + (abs(int(elem[3].split(':')[1]) - int(elem[2].split(':')[1])))
    ) for elem in data]


@r6p
def filter1(data):
    return [elem for elem in data if elem[6] < 45]


@r6p
def reduce_transformer1(data):
    return [((elem[4], elem[5]), elem[6]) for elem in data]


def reduce1(data):
    data = reduce_transformer1(data)

    # return [(key, reduce(lambda obj1, obj2: obj1[1] + obj2[1], group)) for key, group in groupby(sorted(data), key=lambda x: x[0])]
    return [reduce(lambda obj1, obj2: obj1[1] + obj2[1], group) for key, group in groupby(sorted(data), key=lambda x: x[0])]
    

ops_list = [
    read,
    map1,
    map2,
    filter1,
    reduce1,
]