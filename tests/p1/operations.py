# Add r6p to PATH
import sys
sys.path.insert(1, '../../')

from r6p.decorator import r6p


def read(filename, delimiter=','):
    with open(filename) as file:
        lines = []
        for line in file.readlines():
            line = line.strip()
            line = line.split(delimiter)
            lines.append(tuple(line))
            
        return lines
    
@r6p
def map1(data):
    return [(elem[0], elem[1], elem[2].split(), elem[3]) for elem in data]

@r6p
def map2(data):
    return [(elem[0], elem[1], elem[2], int(float(elem[3]))) for elem in data]

@r6p
def filter1(data):
    return [elem for elem in data if int(elem[1]) > 750]

@r6p
def filter2(data):
    return [elem for elem in data if len(elem[2][1]) > 3]

ops_list = [
    read,
    map1,
    map2,
    filter1,
    filter2,
]