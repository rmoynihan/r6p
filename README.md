# R6P
#### Runtime Recovery and Retroactive Repairs for Really Robust Pipelines

Methodology and tool for data pipeline robustness, with the ability to gracefully handle data-related runtime errors as well as generate test cases from new errors it encounters.


## How it Works
#### Runtime Recovery
For any common dataflow operation (e.g. map, filter), simply add the @r6p decorator to the function and let R6P do all the work! R6P will catch any errors at the element level and attempt to diagnose the cause and apply a correction in real time. If R6P is unable to correct the error, it will quarantine the unrecovered record in order to be manually reinspected and possibly fixed/rerun.

#### Retroactive Repairs
Any errors that R6P encounters during runtime, regardless of whether or not they are corrected, will be logged when R6P notices them. At the end of execution, R6P will then review the logs and use them to generate new test cases for the pipeline that can then be incorporated into the test suite. This way, future changes to the pipeline code will need to correct for the error to avoid R6P needing to intervene in the first place. R6P will attempt to consolidate these test records in order to minimize use of test execution time and resources.


## Motivation
Testing data pipelines is inherently a nondeterministic game. Since the developer often has little or no control over the pipeline's input, it is very likely that edge cases will (rarely) pop up that were not tested for to begin with, as a developer can only anticipate so many possible errors. Therefore, R6P takes that approach that tests for these edge cases should write themselves, so that the developer doesn't have to stress over whether or not their test suite is completely comprehensive.

## Running the Project
#### Data Generation
The first step to running any test project is to generate data for it to operate on. This is done with the DataGenerator class located in data_generator.py. To generate data, instantiate the DataGenerator class with the schema that aligns with the needed dataset, using the custom data type classes defined in data_types.py. Then, run the generate_data() method of the DataGenerator, specifying the output filename, the number of test records, and the "mutation_factor", or what percentage of test records will have one of the six mutations applied.

The data generator code for the test project P1 is below:  
`dg = DataGenerator([
        String(prefix="123", string_len=7),
        Integer(0, 1000),
        String(string_type="sentence"),
        Float(0.0, 1.0),
    ])`

`dg.generate_data("generated_data.csv", n_records=1000, m_factor=0.2)`

#### Running a Test Project
First, ensure that your generated test data is in the directory of the project for which you wish to run. Name the data "input.csv", or rename the input file in the project's runner.py. Then, simply run the runner.py script with Python and let the tool do its work. When done, it will output the final result of the program (application specific), as well as generate the logfile.txt and the quaratined.json files.