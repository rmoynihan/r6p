import logging
import datetime
import time
import json

from .recovery import handlers
from .error_type import determine_error_type
from .quarantine import Quarantine
from .tests import RuntimeTestCollector


logging.basicConfig(
    filename="logfile.txt",
    filemode='w',
    format='%(levelname)-8s :: %(message)s'
)
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

quarantine = Quarantine()

rtc = RuntimeTestCollector()

metadata = {'num_errors': 0, 'num_recovered': 0}

def r6p(func):
    """Decorator function that implements the monitoring, recovery, and test generation for r6p"""

    def wrapper(*args, **kwargs):

        def try_itr(data):
            """Attempt to run function on each individual element"""

            logger.info(f'Executing operation {func.__name__}')

            # Create generator that mimics dataflow operation
            for elem in data:
                try:
                    result = func([elem])
                    
                    # For filter functions, check if something was returned
                    if len(result) == 1:
                        yield result[0]

                # Classify and handle the error
                except Exception as e:
                    # Start time
                    start_time = time.time()
                    
                    # Log
                    logger.info(f'Encountered error {e} during execution of {func.__name__}')
                    logger.info(f'Error on element {elem}')
                    metadata['num_errors'] += 1
                    
                    # Send to RuntimeTestCollector
                    rtc.add_error_elem(e, elem)
                    
                    # Determine error type
                    error_type = determine_error_type(e, elem)
                    logger.info(f'Predicted error type: {error_type}')
                    
                    # Handle the error
                    recovered_elem = handlers[error_type](e, elem)
                    logger.debug(f'Recovery object: {recovered_elem}')
                    
                    # Attempt to rerun recovered data
                    try:
                        logger.info(f'Retrying operation with recovered element {recovered_elem}')
                        result = func([recovered_elem])
                        
                        logger.info(f'Successfully recovered error {e} for element')
                        logger.info(f'{elem} ==> {recovered_elem}')
                        metadata['num_recovered'] += 1

                        # Filter check
                        if len(result) == 1:
                            logger.info(f'Took {time.time() - start_time}s')
                            yield result[0]
                    
                    # Fails again
                    except Exception as e:
                        logger.info(f'Could not recover error {e} for element {elem}')
                        quarantine.quarantine(elem, e, error_type)
                        logger.info(f'Took {time.time() - start_time}s')
                            

            logger.info(f'Completed execution of operation {func.__name__}')


        # Cleanup
        quarantine.write_data()
        rtc.save_error_elems()
        
        # Write metadata
        with open('run_metadata.json', 'w') as f:
            json.dump(metadata, f, indent=4)

        # Return the results
        return (list(try_itr(*args)))

    # Add undecorated function back for comparison
    wrapper.unwrapped = func
    
    # Return
    return wrapper