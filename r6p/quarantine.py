import json

class Quarantine:
    """Class that saves data that was unable to be recovered"""
    
    def __init__(self, filename='quarantined.json'):
        self.filename = filename
        self.data = []
        
        open(self.filename, 'w').close()

    def quarantine(self, elem, e, error_type):
        self.data.append({
            'element': elem,
            'predicted_error_type': error_type,
            'interpreter_exception': str(e),
        })
        

    def write_data(self):
        # Read in existing values
        with open(self.filename, 'r') as qfile:
            try:
                existing_data = json.loads(qfile.read())
            except json.decoder.JSONDecodeError:
                existing_data = []

        # Output all quarantine values
        with open(self.filename, 'w') as qfile:
            json.dump(existing_data + self.data, qfile, indent=4)