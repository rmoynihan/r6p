import random

from .data_types import Integer, Float, String


class Mutator:
    """Holds the mutation functions that can be applied to records"""

    def __init__(self, schema):
        self.schema = schema
        self.mutations = [
            self.m1,
            self.m2,
            self.m3,
            self.m4,
            self.m5,
            self.m6,
        ]
        
        
    def random_mutation(self, record):
        """Apply a random mutation to the record"""

        i = random.randint(0, 5)
        return self.mutations[i](record), f'm{i + 1}'
    

    def m1(self, record):
        """
        Data Distribution Mutation
        Mutate numeric records to be beyond their min or max
        """
        min_number = -2147483648
        max_number = 2147483647
        
        record_values = record.values
        
        # Find numeric cols with any min or max restrictions
        numeric_cols = [i for i, col in enumerate(record_values) if type(col) == int or type(col) == float]
        try:
            cols_with_min_or_max = [i for i in numeric_cols if self.schema[i].min_val != min_number or self.schema[i].max_val != max_number] 
        except:
            for i in numeric_cols:
                print(i)
                print(self.schema[i])
        
        # No viable columns
        if len(cols_with_min_or_max) == 0:
            record.values = record_values
            return record
        
        # Pick random col and mutate it
        i = random.choice(cols_with_min_or_max)
        col_to_mutate = self.schema[i]
        col_type = type(record_values[i])
        
        # Has both min and max restrictions; choose one to alter
        if col_to_mutate.min_val != min_number and col_to_mutate.max_val != min_number:
            if random.random() > 0.5:
                record_values[i] = random.uniform(min_number, col_to_mutate.min_val)
            else:
                record_values[i] = random.uniform(col_to_mutate.max_val, max_number)

        # Min restriction only
        elif col_to_mutate.min_val != min_number:
            record_values[i] = random.uniform(min_number, col_to_mutate.min_val)
            
        # Max restriction only
        elif col_to_mutate.max_val != max_number:
            record_values[i] = random.uniform(col_to_mutate.max_val, max_number)

        # Change type back to int if it was so originally
        if col_type == int:
            record_values[i] = int(record_values[i])
            
        record.values = record_values
        return record
    
    
    def m2(self, record):
        """
        Data Type Mutation
        Change the data type of a column
        """

        record_values = record.values

        # Select column to mutate
        i = random.choice(list(range(len(self.schema))))
        
        if type(record_values[i]) == int:
            if random.random() > 0.5:
                record_values[i] = float(record_values[i])
            else:
                record_values[i] = str(record_values[i])
                
        elif type(record_values[i]) == float:
            if random.random() > 0.5:
                record_values[i] = int(record_values[i])
            else:
                record_values[i] = str(record_values[i])
                
        elif type(record_values[i]) == str:
            if random.random() > 0.5:
                record_values[i] = random.randint(0, 1000)
            else:
                record_values[i] = random.uniform(0, 1000)
                
        record.values = record_values
        return record
    

    def m3(self, record):
        """
        Data Format Mutation
        Changes a comma-separated line into another type of delimiter
        """
        sep_chars = ['~', '\t', '|', '\035', ',']

        record.delimiter = random.choice(sep_chars)
        return record
    

    def m4(self, record):
        """
        Data Column Mutation
        Insert one or several characters (see BigFuzz paper pg. 6)
        """

        for i in range(1, random.randint(2, 4)):
            record.append_value(record.delimiter)
            
        return record
    

    def m5(self, record):
        """
        Null Data Mutation
        Remove one or more columns
        """
        
        for i in range(0, min(len(record.values), random.randint(2, 4))):
            record.values.pop(random.randint(0, len(record.values) - 1))

        return record


    def m6(self, record):
        """
        Empty Data Mutation
        Mutate random column to an empty string
        """

        record.values[random.randint(0, len(record.values) - 1)] = ''
        return record