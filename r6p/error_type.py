def determine_error_type(e, elem):
    """
    Given an exception, determine what kind of error (as described in BigFuzz) has occurred
    Logic was empirically derived by studying common error patterns

    Codes:
    - 'TYPE_MISMATCH'
    - 'INC_COLUMN_ACCESS'
    - 'SPLIT_ERROR'
    - 'OTHER'
    """

    e_type = 'OTHER'

    if isinstance(e, IndexError):
        if "tuple" in str(e):
            if len(elem) != 1:
                e_type = 'INC_COLUMN_ACCESS'
            else:
                e_type = 'SPLIT_ERROR'
    
    elif isinstance(e, ValueError):
        e_type = 'TYPE_MISMATCH'

    return e_type