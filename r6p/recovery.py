import re

def recover_inc_column_access(e, elem):
    return tuple(elem)
    

def recover_split_error(e, elem):
    sep_chars = ['~', '\t', '|', '\035', ',']

    new_elem = elem
    for char in sep_chars:
        if elem[0].count(char) >= 1:
            new_elem = elem[0].split(char)

    return tuple(new_elem)


def recover_type_mismatch(e, elem):
    new_elem = []
    for val in elem:
        new_val = val
        try:
            new_val = re.sub(r'[^0-9.]', '', val).strip()

            if len(new_val) > 0:
                if '.' in new_val:
                    new_val = float(val)
                else:
                    new_val = int(val)
            else:
                new_val = val
        except:
            pass
        finally:
            new_elem.append(new_val)

        return tuple(new_elem)

def recover_other(e, elem):
    return tuple(elem)


handlers = {
    'INC_COLUMN_ACCESS': recover_inc_column_access,
    'SPLIT_ERROR': recover_split_error,
    'TYPE_MISMATCH': recover_type_mismatch,
    'OTHER': recover_other,
}