import json


class RuntimeTestCollector:
    def __init__(self, filename='testcases.json'):
        self.filename = filename
        self.error_elems = []
    

    def add_error_elem(self, e, elem):
        # Extract error message minus specific details
        try:
            e = str(e).split(':')[0]
        except:
            e = str(e)

        self.error_elems.append((e, elem))
        

    def characterize_error_elems(self):
        """Create features based on element in order to characterize test"""

        error_elems = []
        for elem in self.error_elems:
            print(elem)
            e = elem[0]
            
            # Ensure all cols are in str form
            elem = ','.join([str(col) for col in elem[1]])
            
            # Determine delimiter
            try:
                delim = [d for d in ['~', '\t', '|', '\035', ','] if len(elem.split(d)) > 1][0]
            except Exception:
                delim = 'unknown'
                
            # Length of elem
            length = len(elem.split(delim))

            error_elems.append({
                'elem': elem,
                'error_msg': e,
                'delimiter': delim,
                'length': length,
            })
            
        self.error_elems = error_elems
    

    def consolidate_error_elems(self):
        """Reduce the suite of error elems to one per unique group based by characterization"""
    
        # Read in existing test cases, if applicable
        try:
            with open(self.filename) as f:
                consolidated = json.loads(f.read())
        except FileNotFoundError:
            consolidated = []

        # Find duplicates based on characterizations
        for elem in self.error_elems:
            is_unique = True

            for i in range(len(consolidated)):
                c = consolidated[i]
                for key in c.keys():
                    if len(set(list(elem.values()) + list(c.values()))) == len(list(elem.values())) + 1:
                        is_unique = False
            
            if is_unique:
                consolidated.append(elem)
                
        # Remove duplicates
        consolidated = [dict(t) for t in {tuple(sorted(c.items())) for c in consolidated}]
                        
        self.error_elems = consolidated
            
    def save_error_elems(self):
        """Save the resulting elements, if there isn't already one that is saved representing that element's group"""
        self.characterize_error_elems()
        self.consolidate_error_elems()
        # print(self.error_elems)
        
        # Write
        with open(self.filename, 'w') as outfile:
            json.dump(self.error_elems, outfile, indent=4)
        
        # Reset error elems
        self.error_elems = []