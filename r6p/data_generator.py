import csv
import random
from collections import defaultdict
import json

from .data_types import Integer, Float, String, Date, Time, Concat
from .mutations import Mutator


class DataGenerator:
    """
    Generates mutated test data sets for abstracted DISC apps
    Adapted from BigFuzz, Zhang et. al.
    """

    def __init__(self, schema):
        self.schema = schema
        self.mutator = Mutator(schema)

    def generate_record(self):
        record = Record()
        for col in self.schema:
            record.append_value(col.generate_value())

        return record

    def generate_data(self, n_records=1000, filename="generated_data.csv", m_factor=0.1, create_metadata=True):
        records = []
        metadata = defaultdict(int)
        for i in range(n_records):
            record = self.generate_record()
            
            # Randomly decide to mutate
            if random.random() <= m_factor:
                record, mutation_label = self.mutate_record(record)
                
                metadata['total_mutation_count'] += 1
                metadata[f'{mutation_label}_count'] += 1
                
            # Add record to record list
            records.append(record)
            
        self.write_data(records, metadata, filename, create_metadata)
        
    def mutate_record(self, record):
        return self.mutator.random_mutation(record)
        

    def write_data(self, records, metadata, filename, create_metadata):
        with open(filename, 'w') as f:
            for record in records:
                line = record.delimiter.join([str(val) for val in record.values])
                f.write(line)
                f.write('\n')
                
        if create_metadata:
            with open(f'{filename.split(".csv")[0]}_metadata.json', 'w') as f:
                json.dump(metadata, f, indent=4)
        

class Record:
    """Represents a single record (or row) in a CSV file"""

    def __init__(self, values=None, delimiter=','):
        if values is None:
            self.values = []
        else:
            self.values = values

        self.delimiter = delimiter
        
    def append_value(self, value):
        self.values.append(value)
        
    def __str__(self):
        return str(self.values) + f', delimiter: {self.delimiter}'

if __name__ == "__main__":
    pass
    # # Individual mutation tests
    # print(dg.mutator.m1(dg.generate_record()))
    # print(dg.mutator.m2(dg.generate_record()))
    # print(dg.mutator.m3(dg.generate_record()))
    # print(dg.mutator.m4(dg.generate_record()))
    # print(dg.mutator.m5(dg.generate_record()))
    # print(dg.mutator.m6(dg.generate_record()))