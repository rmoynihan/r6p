import random
import string
import lorem
import datetime

class Integer:
    """An int column, with optional min and max values"""

    def __init__(self, min_val=None, max_val=None):
        if min_val is not None:
            self.min_val = min_val
        else:
            self.min_val = -2147483648

        if max_val is not None:
            self.max_val = max_val
        else:
            self.max_val = 2147483647
        
    def generate_value(self):
        return random.randint(self.min_val, self.max_val)


class Float:
    """A float column, with optional min and max values"""

    def __init__(self, min_val=None, max_val=None):
        if min_val is not None:
            self.min_val = min_val
        else:
            self.min_val = -2147483648.0

        if max_val is not None:
            self.max_val = max_val
        else:
            self.max_val = 2147483647.0
        
    def generate_value(self):
        return round(random.uniform(self.min_val, self.max_val), 5)


class String:
    """String data type. Can specify word (with pre/post fix and fixed len), sentence, or paragraph"""

    def __init__(self, string_type="word", prefix=None, postfix=None, string_len=None, char_type="alphanumeric"):
        self.string_type = string_type

        # Set options if the string is only one "word"
        if self.string_type == "word":
            # Create prefix
            if prefix:
                self.prefix = prefix
            else:
                self.prefix = ''
                
            # Create postfix
            if postfix:
                self.postfix = postfix
            else:
                self.postfix = ''

            # Set string length
            self.string_len = string_len
            
            # Set list of possible chars to choose from
            if char_type == "alpha_only":
                self.possible_chars = list(string.ascii_letters)
            elif char_type == "numeric_only":
                self.possible_chars = [str(i) for i in range(10)]
            elif char_type == "alphanumeric":
                self.possible_chars = list(string.ascii_letters) + [str(i) for i in range(10)]
            
    def generate_value(self):
        text = ""
        if self.string_type == "word":
            # If string len not defined, randomly determine it
            if not self.string_len:
                string_len = random.randint(1, 10)
            else:
                string_len = self.string_len
            
            # Add prefix
            text += self.prefix
            
            # Generate characters
            while len(text) < string_len:
                text += random.choice(self.possible_chars)

            # Add postfix
            text += self.postfix
            
        elif self.string_type == "sentence":
            text = lorem.sentence()
        
        else:
            text = lorem.paragraph()
            
        return text
    

class Date:
    """Date data type. Specify format and start/end date."""

    def __init__(self, date_format='%m-%d-%Y', start_date=None, end_date=None):
        self.date_format = date_format

        if not start_date:
            self.start_date = datetime.datetime(1900, 1, 1)
        else:
            self.start_date = datetime.datetime.strptime(start_date, date_format)
            
        if not end_date:
            self.end_date = datetime.datetime(2020, 12, 31)
        else:
            self.end_date = datetime.datetime.strptime(end_date, date_format)

    def generate_value(self):
        delta = self.end_date - self.start_date
        rand_days = random.randrange(delta.days)

        rand_date = self.start_date + datetime.timedelta(days=rand_days)

        return rand_date.strftime(self.date_format)


class Time:
    """Time data type"""

    def __init__(self, military=True, has_meridiem=False):
        self.military = military
        self.has_meridiem = has_meridiem

    def generate_value(self):
        time = ""
        
        if self.military:
            time += str(random.randint(0, 23)).zfill(2)
        else:
            time += str(random.randint(0, 12)).zfill(2)
            
        time += ':'
        
        time += str(random.randint(0, 60)).zfill(2)

        if self.has_meridiem:
            time += random.choice(['AM', 'PM'])

        return time
        

class Concat:
    """Concat two or more data types"""

    def __init__(self, values):
        self.values = values
        
    def generate_value(self):
        result_str = ''
        for v in self.values:
            try:
                result_str += str(v.generate_value())
            except AttributeError:
                result_str += str(v)

        return result_str